<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Mvdnbrk\EloquentExpirable\Expirable;
use App\Otp;

class CreateOtpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {
        Schema::create('otps', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('otp_code', 6);
            $table->expires('expired_at'); //The Expirable trait will automatically cast the "expired_at" attribute to a "DateTime / Carbon" instance for you.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otps');
        //Schema::dropExpires();
    }
}
