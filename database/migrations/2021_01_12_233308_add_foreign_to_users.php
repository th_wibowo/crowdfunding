<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Role;

class AddForeignToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->uuid('roles_id')->nullable();
          $table->foreign('roles_id')->references('id')->on('roles')->onDelete('cascade');

          $table->uuid('otps_id')->nullable();
          $table->foreign('otps_id')->references('id')->on('otps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->dropForeign(['roles_id']);
          $table->dropColumn(['roles_id']);

          $table->dropForeign(['otps_id']);
          $table->dropColumn(['otps_id']);
        });
    }
}
