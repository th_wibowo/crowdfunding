<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Role::create([
          'id' => Str::uuid(),
          'role_name' => 'user',//or admin
      ]);
      Role::create([
          'id' => Str::uuid(),
          'role_name' => 'admin',
      ]);
    }
}
