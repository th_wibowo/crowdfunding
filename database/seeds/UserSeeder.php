<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Otp;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   //create 6 user and ave them to DB
        factory(User::class, 2)->create();
        //otpfactory(Otp::class, 5)->create();

    }
}
