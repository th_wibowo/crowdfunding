<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//
// //rote hasil AUTH
// Auth::routes();
//
// Route::get('/home', 'HomeController@index')->name('home');
//route hasil AUTH

//route untuk tugas#3 NEED IMPROVEMENT
// Route::middleware(['auth'])->group(function(){
//
//   Route::get('/homepage/admin', 'VerificationController@admin');//->middleware('accessMiddleware');//halaman admin
//   Route::get('/homepage', 'VerificationController@user');//halaman default
//
// });

//rute1
// Route::get('/route-1', function () {
//     return view('userpage');//return 'masuk'; akan diarahkan ke halaman bertuliskan tulisan tersebut, jika lolos auth
// })->middleware(['auth', 'emailMiddleware']);
// //rute2
// Route::get('/route-2', function () {
//     return view('adminpage');
// })->middleware(['auth', 'emailMiddleware', 'accessMiddleware']);

// Route::get('/', function(){
//   return view('app');
// });

// Route::get('/{any?}', function(){
//   return 'masuk ke sini';
// })->where('any', '.*');//agar pada subroute jg bisa diakses melalui any url

Route::view('/{any?}', 'app')->where('any', '.*');
