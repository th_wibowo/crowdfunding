<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ url('/css/app.css') }}">
  </head>
  <body>

    <div id="app">
      <app>
      </app>
    </div>
    <script src="{{ url('/js/app.js') }}"></script>

  </body>
</html>
