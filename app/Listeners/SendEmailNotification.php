<?php

namespace App\Listeners;

use App\Events\UserRegisteredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\UserRegisteredMail;
use Mail;

class SendEmailNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisteredEvent  $event
     * @return void
     */
    public function handle($event)//karena yg menerima adalah $event
    {//sehingga ditulis $event->user

        if ($event->condition == 'register'){
          $pesan = "We're excited to have you get started. First, you need to confirm your account. Here's your OTP Code: ";
        }
        elseif ($event->condition == 'regenerate'){
          $pesan = "Regenerate OTP successfull. Here is your OTP Code: ";
        }

        Mail::to($event->user)->send(new UserRegisteredMail($event->user, $pesan));
    }
}
