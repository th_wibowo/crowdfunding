<?php

namespace App\Traits;
use Illuminate\Support\Str;

trait Uuid{//dalam satu model hanya boleh ada satu method boot()
  protected static function boot() {
     parent::boot();//jika tidak dicantumkan model User tidak akan dikenali
     static::creating(function ($model) {
         if ( ! $model->getKey()) {
             $model->{$model->getKeyName()} = (string) Str::uuid();
         }
     });
  }

  //Menambahkan bagian ini untuk menghilangkan autoincrement
  public function getIncrementing()
  {
     return false;
  }

  /**
  * Get the auto-incrementing key type.
  *
  * @return string
  */
  public function getKeyType()
  {
     return 'string';
  }
}

?>
