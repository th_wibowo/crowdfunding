<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
class Campaign extends Model
{
    use Uuid;

    protected $guarded = [];
}
