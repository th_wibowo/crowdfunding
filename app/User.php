<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use App\Traits\Uuid;
use App\Role;
use App\Otp;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, Uuid;//panggil traitnya disini, tambahkan "use Uuid;"

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected function get_user_role_id(){
      $role = Role::where('role_name', 'user')->first();//first() untuk mendapatkan data dalam array collection dengan urutan pertama
      return $role->id;
    }

    //jika sudah ada method boot() tidak perlul agi meng-import Trait dgn use nam_trait
    public static function boot(){
      parent::boot();
      //membuat uuid dan mengisi field id pada model User, saat proses insert
      static::creating(function ($model) {
          if ( ! $model->getKey()) {
              $model->{$model->getKeyName()} = (string) Str::uuid();
          }
      });
      static::creating(function ($model) {//when creating event
          //mengisi field roles_id secara otomatis sesuai dengan id pada tabel role, saat proses insert
          $model->roles_id = $model->get_user_role_id();
      });

      static::created(function ($model) {//when created events
        $model -> generate_otp_code();
      });
    }

    protected $fillable = [
        'name', 'email', 'password', 'photo_profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
//
    public function isAdmin(){

      if($this->role_id === $this->get_user_role_id()){
        return false;
      }
      return true;

    }

    public function generate_otp_code(){
      do{
        $random = mt_rand(100000,999999);
        $check = Otp::where('otp_code' , $random)->first();

      }while ($check);

      date_default_timezone_set('Asia/Jakarta');

      $now = Carbon::now();

      //create otp code
      $otp_code = Otp::updateOrCreate(
          ['users_id' => $this->id],
          ['otp_code' => $random, 'expired_at' => $now->addMinutes(5)]
      );
    }

    //kode wajib JWT
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    //kode wajib JWT

    public function role(){
      return $this->hasOne(Role::class);//atau hasOne('App\Role');
    }

    public function otp_fun(){
      return $this->hasOne('App\Otp', 'users_id', 'id');//class_name, Foreign_key_name, id
    }

}
