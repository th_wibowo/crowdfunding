<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mvdnbrk\EloquentExpirable\Expirable;
use Illuminate\Support\Str;
use App\Traits\Uuid;
use App\User;

class Otp extends Model
{
  use Uuid, Expirable;

  protected $table = "otps";

  // protected $fillable = [
  //     'otp_code', 'users_id',
  // ];
  protected $guarded = [];

  public function user(){
    return $this->belongsTo(User::class, 'users_id');
  }
}
