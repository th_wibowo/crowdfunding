<?php

namespace App;
use App\User;
use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
  use Uuid;
  protected $table = "roles";
  protected $guarded = [];

  public function user_one(){
    return $this->belongsTo(User::class, 'users_id');
  }

  public function user_many(){
    return $this->hasMany(User::class);//atau hasMany('App\User');
  }

}
