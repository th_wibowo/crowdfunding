<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Otp;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user, $pesan;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $pesan)//menerima parameter ketika halaman ini diinstasiasi dengan model User
    {
        $this->user = $user;
        $this->pesan = $pesan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.otp_code')
                    //parameter yg akan dikirim ke view
                    ->with([
                            'user' => $this->user,//mengambil value user
                            'pesan' => $this->pesan,
                        ]);
    }
}
