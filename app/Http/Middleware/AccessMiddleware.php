<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(Auth::user()->isAdmin()){//jika user adalah admin, maka akses ke halaman berikutnya diberikan
        return $next($request);//
      }
      abort(503);

      //atau dengan
      // return response()->json([
      //   'message' => 'You are not an ADMIN, access denied!',
      // ]);
    }
}
