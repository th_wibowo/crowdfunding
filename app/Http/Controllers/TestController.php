<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
Use Auth;

class TestController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  
  public function store(Request $request){
    $request->validate([
      'judul' => 'required|unique:pertanyaan',
      'isi' => 'required'
    ]);

    //Menggunakan Eloquent ORM
    // $modelVar = new FirstModel;
    // $modelVar -> judul = $request["judul"];
    // $modelVar -> isi = $request["isi"];
    // $modelVar -> save(); //insert into pertanyaan(judul, isi) values ("judul", "isi");

    //Menggunakan MASS ASSIGNMENT atau GUARDED
    $modelVar = User::create([
      "judul" => $request["judul"],
      "isi" => $request["isi"]
    ]);

    return redirect('/home')->with('success', 'Pertanyaan berhasil disimpan! :)');
  }
}
