<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Otp;
use Carbon\Carbon;
use App\User;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
      $request->validate([
        'otp_code'  => 'required',
      ]);

      $otp_code = Otp::where('otp_code', $request->otp_code)->first();

      if(!$otp_code){
        return response()->json([
          'Response_code' => '01',
          'Response_message' => 'Kode OTP tidak ditemukan',
        ], 200);
      }

      $now = Carbon::now();//mengambil waktu sekarang dengan menggunakan Carbon

      if($now > $otp_code->expired_at){
        return response()->json([
          'Response_code' => '01',
          'Response_message' => 'Kode OTP sudah tidak berlaku, silahkan generate ulang',
        ], 200);
      }

      //update users
      $user = User::find($otp_code->users_id);
      $user->email_verified_at = Carbon::now();
      $user->save();

      //delete otp_code
      $otp_code->delete();

      $data['user'] = $user;

      return response()->json([
        'Response_code' => '00',
        'Response_message' => 'User telah berhasil diverifikasi',
      ], 200);
    }

}
