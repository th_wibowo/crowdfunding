<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
      $request->validate([
        'email' => 'required',
        'password'  => 'required',
      ]);

      $credentials = $request->only(['email', 'password']);

      if(! $token = auth()->attempt($credentials)){//jika setting di default guard = web pada config\auth.php, maka ditulis auth('api')
          return response()->json(['error' => 'Email unknown, or Password not valid'], 401);
      }

      $data['token'] = $token; //menambahkan value token dan nama user pada json
      $data['user'] = auth()->user();

      return response()->json([
        'response_code' => '00',
        'response_message' => 'User telah berhasil login',
        'data'  => $data
      ], 200);
    }
}
