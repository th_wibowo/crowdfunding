<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Events\UserRegisteredEvent;
use App\User;
use App\Otp;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
      $request->validate([
        'email'  => 'required',
      ]);

      $user = User::where('email', $request->email)->first();

      $user->generate_otp_code();//method yg berasal dari User.php

      event(new UserRegisteredEvent($user, 'regenerate'));

      $data['user'] = $user;

      return response()->json([
        'Response_code' => '00',
        'Response_message' => 'Kode OTP berhasil di-generate, silahkan cek email anda',
        'Data' => $data
      ]);
    }
}
