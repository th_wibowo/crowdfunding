<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Events\UserRegisteredEvent;
use App\User;
use App\Otp;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {   $data = [];
        $user = User::create([
          'name' => request('name'),
          'email' => request('email'),
          'password' => bcrypt(request('password')),
        ]);
        // $request->validate([
        //   'name'  => 'required',
        //   'email' => 'required|unique:users,email|email',//email yg terakhir untuk mengharusakn format penulisan kolom seperti email asli
        // ]);
        // $data_request = $request->all();
        // $user = User::create($data_request);

        event(new UserRegisteredEvent($user, 'register'));

        $data['users'] = $user;

        return response()->json([
          'Response_code' => '00',
          'Response_message' => 'User baru telah berhasil didaftarkan, silahkan cek email untuk melihat kode OTP',
          'Data' => $data
        ]);
    }
}
