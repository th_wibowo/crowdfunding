<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Blog extends Model
{
    use Uuid;

    protected $guarded = [];
}
